RED=\033[0;31m
GREEN=\033[1;32m
PURPLE=\033[0;35m
ORANGE=\033[0;33m
BLUE=\033[0;34m
NC=\033[0m

all:
	@echo "${PURPLE}Usage: make <rule>${NC}"; \
	echo "${ORANGE}PRODUCTION${NC}"; \
	echo "${BLUE}Deploy:${NC}"; \
	echo "deploy"; \
	echo "${BLUE}\nCaddy:${NC}"; \
	echo "caddy_start"; \
	echo "caddy_stop"; \
	echo "caddy_reload"; \
	echo "${BLUE}\nDocker:${NC}"; \
	echo "check_containers"; \
	echo "restart_container"; \
	echo "${BLUE}\nFiles management:${NC}"; \
	echo "copy_remote_file"; \
	echo "copy_index"; \
	echo "copy_caddy_conf"; \
	echo "copy_monitoring_script"; \
	echo "copy_matcha"; \
	echo "copy_transcendence"; \
	echo "${ORANGE}\nDEVELOPMENT${NC}"; \
	echo "client_up"; \
	echo "server_up"; \
	echo "client_test"; \
	echo "client_test_coverage"; \
	echo "clean"; \
	echo "fclean"; \
	echo "prune"; \
	echo ""; \

# PROD
#####################################################################
deploy:
	ansible-playbook -i deploy/inventory.ini deploy/deploy.yaml

caddy_start:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/start_caddy.yaml

caddy_stop:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/stop_caddy.yaml

caddy_reload:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/reload_caddy.yaml

check_containers:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/docker/check_containers.yaml

restart_container:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/docker/restart_container.yaml

copy_remote_file:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/copy_remote_file.yaml

copy_index:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/copy_file/index.yaml

copy_caddy_conf:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/copy_file/caddy_conf.yaml

copy_monitoring_script:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/copy_file/monitoring.yaml

copy_matcha:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/copy_file/matcha.yaml

copy_transcendence:
	ansible-playbook -i deploy/inventory.ini deploy/playbooks/copy_file/transcendence.yaml

# DEV
#####################################################################
client:
	docker compose up client --build

server:
	docker compose up server --build

# Lance les tests unitaires pour le client
client_test:
	docker compose run client sh -c "cd /usr/src/app && npm run test"

# Check la couverture des test unitaires pour le client
client_test_coverage:
	docker compose run --rm client sh -c "cd /usr/src/app && npm run coverage"

# Supprime les containers, network et volumes.
clean:
	docker-compose down -v

# Supprime les containers, network volumes et images.
fclean:
	docker-compose down --rmi all -v

# fclean + remove cache
prune: fclean
	docker system prune -af && docker volume prune

.PHONY: all \
	deploy \
	caddy_start \
	caddy_stop \
	caddy_reload \
	check_containers \
	restart_container \
	copy_remote_file \
	copy_index \
	copy_caddy_conf \
	copy_monitoring_script \
	copy_matcha \
	copy_transcendence \
	client_up \
	server_up \
	client_test \
	client_test_coverage \
	clean \
	fclean \
	prune
