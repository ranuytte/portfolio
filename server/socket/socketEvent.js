import pty from 'node-pty';

export default function socketEvent(io) {

    io.on("connection", (socket) => {
        console.log("Socket connected: " + socket.id);

        var ptyProcess = pty.spawn('bash', [], {
            name: 'xterm-256color',
            cols: 80,
            rows: 30,
            cwd: process.cwd(),
            // cwd: "/",
            env: process.env,
        });

        socket.on('cmdInput', command => {
            var processedCommand = commandProcessor(command)
            ptyProcess.write(processedCommand);
        });
        
        socket.on('interruptCommand', () => {
            console.log("Interrupt command")
            // ptyProcess.kill('SIGINT');
            ptyProcess.kill('SIGKILL');
        });

        // ptyProcess.onExit(() => {
        //     console.log("test");
        // })

        // Output: Sent to the frontend
        ptyProcess.on('data', function (rawOutput) {
            var processedOutput = outputProcessor(rawOutput);
            socket.emit('cmdOutput', processedOutput);
        });

        // TODO: Faire un filter pour un array de command
        const commandProcessor = function(command) {
            console.log("Command: " + JSON.stringify(command))
            const bannedCmd = 'yes';
            if (command.includes(bannedCmd))
                return command.replaceAll(bannedCmd, "invalid_command");
            return command;
        }

        const outputProcessor = function(output) {
            return output;
        }

        socket.on("disconnecting", () => {
            console.log(socket.rooms); // the Set contains at least the socket ID
        });

        socket.on("disconnect", () => {
            console.log("Socket disconnected: " + socket.id)
            ptyProcess.kill();
        });

    });
}
