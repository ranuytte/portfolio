import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import socketEvent from './socket/socketEvent.js';

const hostname = '0.0.0.0';
const port = 5000;

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
    cors: {
        origin: "http://localhost:3000"
    },
    serveClient: false,
});

socketEvent(io);

httpServer.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
