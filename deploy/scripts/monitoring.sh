#! /bin/bash

# MEMORY_USAGE
MEM=$(free --mega | grep 'Mem')
MEM_arr=($MEM)
MEM_USED=${MEM_arr[2]}
MEM_TOTAL=${MEM_arr[1]}
MEM_PCENT=`echo "scale=2;(100*($MEM_USED)/$MEM_TOTAL)" | bc`

# CPU_LOAD
CPU=$(cat /proc/stat | grep -m 1 'cpu')
CPU_arr=($CPU)
CPU_IDLE=${CPU_arr[4]}
CPU_TOTAL=$((${CPU_arr[1]} + ${CPU_arr[2]} + ${CPU_arr[3]} + ${CPU_arr[4]} + ${CPU_arr[5]}\
       	+ ${CPU_arr[6]} + ${CPU_arr[7]} + ${CPU_arr[8]} + ${CPU_arr[9]} + ${CPU_arr[10]}))
CPU_LOAD=`echo "scale=2;(100*($CPU_TOTAL - $CPU_IDLE) / $CPU_TOTAL)" | bc | sed 's/^\./0./'`

# DISK USAGE
DISK_AVAIL=$(df -h --output=avail | sed -n '4p' | sed 's/ //g')
DISK_SIZE=$(df -h --output=size | sed -n '4p' | sed 's/ //g')
DISK_PCENT=$(df -h --output=pcent | sed -n '4p' | sed 's/ //g')

# ARCHITECTURE
ARCH=$(uname -a)

# CPU PHYSICAL
pCPU=$(cat /proc/cpuinfo | grep 'processor' | wc -l)

# CPU VIRTUAL
vCPU=$(lscpu | grep '^CPU(s)' | cut -c34-)

# LAST BOOT
LAST_BOOT=$(who -b | cut -c23-)

# ETABLISHED CONNECTION
TCP=$(netstat -tn | wc -l)
TCP=$(($TCP - 2))

# LVM use
if grep -q "mapper" /etc/fstab ; then
	LVM="Yes"
else
	LVM="No"
fi

# USER LOG
USER_LOG=$(who | wc -l)

# NETWORK
IP=$(hostname -I)
MAC=$(cat /sys/class/net/enp0s3/address)

# SUDO CMD
SUDO=$(cat /var/log/sudo/sudo.log | grep 'COMMAND' | wc -l)

echo -e "#Architecture: $ARCH\n#CPU physical: $pCPU\n#vCPU: $vCPU\n#Memory Usage: "$MEM_USED/$MEM_TOTAL"MB ($MEM_PCENT%)\n#Disk Usage: $DISK_AVAIL/$DISK_SIZE ($DISK_PCENT)\n#CPU_LOAD: $CPU_LOAD%\n#LAST_BOOT: $LAST_BOOT\n#LVM USE: $LVM\n#Connexions TCP (etablished): $TCP\n#User log: $USER_LOG\n#Network: IP $IP ($MAC)\n#Sudo: $SUDO\n" | wall


