import { Terminal } from 'xterm';

export interface xtermCommand {
    id: string,
    args: number,
    description: string,
    exec: (term: Terminal, args?: string[]) => void
}
