import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import { fileSystem } from './constant';

const open: xtermCommand = {
    id: 'open',
    args: 1,
    description: 'Open file',
    exec: ((term: Terminal, args?: string[]) => {
        if (!args)
            return;
        let url = "";
        const file = fileSystem.find(file => file.name === args[0]);
        if (file?.name === 'CV.pdf') {
            url = `${window.location.origin}${file.path}`;
        }
        else {
            url = file!.path;
        }

        if (url === "") {
            term.writeln(`"${args[0]}" no such file or application`);
            return;
        }
        term.writeln(`opening ${args[0]}...`);
        setTimeout(() => {
            window.open(url);
        }, 1000)
    })
}

export default open;
