import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import { termColors } from './constant';
import { putColor } from './utils';

const whoami: xtermCommand = {
    id: "whoami",
    args: 0,
    description: "display effective developer info",
    exec: ((term: Terminal) => {
        term.writeln(putColor(termColors.Yellow, "name: ") + "Raphael Nuytten");
        term.writeln(
            putColor(termColors.Yellow, "current position: ") +
                "student"
        );
        term.writeln(putColor(termColors.Yellow, "location: ") + "Rouen, France");
        term.writeln(
            putColor(termColors.Yellow, "fav languages: ") +
                "[Typescript, C++, C]"
        );
        term.writeln(
            putColor(termColors.Yellow, "hobbies: ") +
                "[VIM, cooking, sport]"
        );
        term.writeln(
            putColor(termColors.Yellow, "find me: ") + 
                '\x1b]8;;https://gitlab.com/ranuytte\x07gitlab/ranuytte\x1b]8;;\x07' +
                ' - ' +
                '\x1b]8;;https://www.linkedin.com/in/rapha%C3%ABl-nuytten/\x07linkedin/raphael-nuytten\x1b]8;;\x07'
        );
    })
};

export default whoami;
