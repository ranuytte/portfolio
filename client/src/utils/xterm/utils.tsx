import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import cd from './cd';
import { termColors } from './constant';
import exit from './exit';
import help from './help';
import ls from './ls';
import open from './open';
import uname from './uname';
import whoami from './whoami';

export function putColor(color:string, text:string) {
    return (`${color}${text}${termColors.Reset}`)
}

export const sysCommand: xtermCommand[] = [
    whoami,
    ls,
    cd,
    open,
    uname,
    exit,
    help,
]

export function execCommand(term:Terminal, userInput:string) { 
    const [input, ...args] = userInput.split(/\s+/);
    const command = sysCommand.find((cmd) => cmd.id === input);
    if (!command){
        term.writeln('Command not found. Type "help" to list available commands')
        return
    }
    if (command?.args === 0 && args.length !== 1) {
        term.writeln('Command does not take arguments')
        return;
    }
    if (command?.args === 1 && args.length !== 2) {
        if (command.id === 'cd')
            term.writeln(`usage: ${command.id} [link]`)
        else
            term.writeln(`usage: ${command.id} [file]`)
        return;
    }
    command?.exec(term, args);
}
