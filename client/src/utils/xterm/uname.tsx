import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import { termColors } from './constant';
import { putColor } from './utils';

const uname:xtermCommand = {
    id: 'uname',
    args: 0,
    description: 'display information about the system',
    exec: (term: Terminal) => {
        let info = putColor(termColors.Green, "User Agent: ");
        info += navigator.userAgent + "\r\n";
        info += putColor(termColors.Green, "Host: ");
        info += location.hostname;
        term.writeln(info);
    }
}

export default uname;
