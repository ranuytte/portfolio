import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import { fileSystem, termColors } from './constant';
import { putColor } from './utils';

const ls: xtermCommand = {
    id: 'ls',
    args: 0,
    description: 'List directory content',
    exec: ((term: Terminal) => {
        for (const {name, isLNK} of fileSystem) {
            if (isLNK)
                term.writeln(putColor(termColors.Purple, name))
            else
                term.writeln(putColor(termColors.BBlue, name));
        }
    })
}

export default ls;
