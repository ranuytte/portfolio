export const termColors = {
  Red: "\x1B[31m",
  BRed: "\x1B[1;31m",
  Green: "\x1B[32m",
  Yellow: "\x1B[33m",
  Blue: "\x1B[36m",
  BBlue: "\x1B[1;36m",
  Purple: "\x1B[35m",
  BPurple: "\x1B[1;35m",
  Reset: "\x1B[0m",
};

export const fileSystem = [
    {
        name: 'CV.pdf',
        path: '/download/CV.pdf',
        isLNK: false
    },
    {
        name: 'tetris',
        path: 'https://gitlab.com/ranuytte/42_red_tetris',
        LNK: "TETRIS",
        isLNK: true
    },
    {
        name: 'iot',
        path: 'https://gitlab.com/ranuytte/42_iot',
        LNK: "IOT",
        isLNK: true
    },
    {
        name: 'lfs',
        path: 'https://gitlab.com/ranuytte/42_lfs',
        LNK: "LFS",
        isLNK: true
    },
    {
        name: 'matcha',
        path: 'https://gitlab.com/ranuytte/42_matcha',
        LNK: "MATCHA",
        isLNK: true
    },
    {
        name: 'transcendance',
        path: 'https://gitlab.com/ranuytte/42_transcendence',
        LNK: "TRANSCENDANCE",
        isLNK: true
    },
    {
        name: 'containers',
        path: 'https://gitlab.com/ranuytte/42_containers',
        LNK: "CONTAINERS",
        isLNK: true
    },
    {
        name: 'irc',
        path: 'https://gitlab.com/ranuytte/42_irc',
        LNK: "IRC",
        isLNK: true
    },
    {
        name: 'minishell',
        path: 'https://gitlab.com/ranuytte/42_minishell',
        LNK: "MINISHELL",
        isLNK: true
    },
    {
        name: 'push_swap',
        path: 'https://gitlab.com/ranuytte/42_push_swap',
        LNK: "PUSHSWAP",
        isLNK: true
    },
    {
        name: 'philosophers',
        path: 'https://gitlab.com/ranuytte/42_philosophers',
        LNK: "PHILOSOPHERS",
        isLNK: true
    },
]
