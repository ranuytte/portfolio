import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import { fileSystem, termColors } from './constant';
import { putColor } from './utils';

const cd: xtermCommand = {
    id: 'cd',
    args: 1,
    description: 'Navigate to the correspding section',
    exec: ((term: Terminal, args?: string[]) => {
        if (!args)
            return;
        const project = fileSystem.find(file => file.name === args[0])
        if (!project) {
            term.writeln(`${args[0]} does not exist`);
            return;
        }
        if (project.name === 'CV.pdf') {
            term.writeln(putColor(termColors.Red, '[error]') + ` ${args[0]} is a file`);
            return;
        }
        const monProjetElement = document.getElementById(`${project.LNK}`);
        if (project.name === 'philosophers') {
            monProjetElement?.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
        }
        else {
            monProjetElement?.scrollIntoView({ behavior: "smooth" });
        }
    })
}

export default cd;
