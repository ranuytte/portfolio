import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';

const exit:xtermCommand = {
    id: 'exit',
    args: 0,
    description: 'Exit the website',
    exec: (term: Terminal) => {
        term.writeln('terminating session...');
        setTimeout(() => {
            window.location.href = 'https://c.tenor.com/WMUPl3olKNAAAAAC/tenor.gif';
        }, 1500);
    }
}

export default exit;
