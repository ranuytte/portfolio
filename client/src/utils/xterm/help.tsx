import { Terminal } from 'xterm';
import { xtermCommand } from '../dtos';
import { termColors } from './constant';
import { putColor, sysCommand } from './utils';

const help:xtermCommand = {
    id: 'help',
    args: 0,
    description: 'Show help',
    exec: (term: Terminal) => {
        term.writeln(putColor(termColors.Green, "available commands:"));
        for (const {id, description} of sysCommand) {
            term.writeln("\t" + putColor(termColors.Yellow, id) + "\t" +  description);
        }
    }
}

export default help;
