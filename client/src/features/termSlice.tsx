import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../app/store';

interface terminalState {
    id: string,
    cmdInput: string,
    cmdOutput: string,
    isConnected: boolean
}

export interface terminalRootState {
    terminals: { [id:string]: terminalState}
}

const initialState: terminalRootState = {
    terminals: {}
}

const termSlice = createSlice({
    name: "terminals",
    initialState,
    reducers: {
        initTerm: (state, action) => {
            const { id, cmdInput, cmdOutput, isConnected } = action.payload;
            state.terminals[id] = { id, cmdInput, cmdOutput, isConnected };
        },
        connectionEstablished: (state, action) => {
            state.terminals[action.payload.id].isConnected = true;
        },
        connectionLost: (state, action) => {
            state.terminals[action.payload.id].isConnected = false;
        },
        onData: (_state, _action) => {},
        printPrompt: (_state, _action) => {},
        commandInput: (state, action) => {
            state.terminals[action.payload.id].cmdInput = action.payload.data;
        },
        commandOutput: (state, action) => {
            state.terminals[action.payload.id].cmdOutput = action.payload.data;
        }
    }
});

export const {
    initTerm,
    connectionEstablished,
    connectionLost,
    onData,
    printPrompt,
    commandInput,
    commandOutput,
} = termSlice.actions;

export const getTerminalStatus = (state: RootState) => state.terminals;

export default termSlice.reducer;
