import {combineReducers, configureStore} from '@reduxjs/toolkit';
import logger from 'redux-logger';
import termSlice from '../features/termSlice';
import termMiddleware from '../middleware/termMiddleware';

// Create the root reducer independently to obtain the RootState type
const rootReducer = combineReducers({
    terminals: termSlice
})

export const store = configureStore({
    reducer: rootReducer,
    // @ts-expect-error hell-type!
    middleware: (import.meta.env.PROD === true ? 
                 (getDefaultMiddleware) => getDefaultMiddleware().concat(termMiddleware)
                     : 
                    (getDefaultMiddleware) => getDefaultMiddleware().concat(termMiddleware, logger))
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
