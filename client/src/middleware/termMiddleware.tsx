import { Dispatch, PayloadAction, Store } from '@reduxjs/toolkit';
import { FitAddon } from '@xterm/addon-fit';
import { WebLinksAddon } from '@xterm/addon-web-links';
import ansiEscapes from 'ansi-escapes';
// import { io, Socket } from 'socket.io-client';
import { Terminal } from 'xterm';
import { commandInput, initTerm, onData, printPrompt } from '../features/termSlice';
import { termColors } from '../utils/xterm/constant';
import { execCommand, putColor } from '../utils/xterm/utils';

interface terminalObjet {
    terminal: Terminal;
    // socket: Socket;
    buffer: string;
    prompt: string;
}

export default function termMiddleware(store: Store) {
    const terminals: { [id: string]: terminalObjet } = {};

    return (next: Dispatch) => (action: PayloadAction) => {
        if (initTerm.match(action)) {
            if (!(action.payload.id in terminals)) {
                terminals[action.payload.id] = { 
                    terminal: new Terminal({
                        fontFamily: '"Cascadia Code", Menlo, monospace',
                        theme: baseTheme,
                        cursorBlink: true,
                        cols: 80,
                        rows: (window.innerHeight > 800 ? 30 : 20)
                    }),
                    // socket: io("http://0.0.0.0:5000"),
                    buffer: "",
                    prompt: putColor(termColors.Blue, "user@terst:~$ ")
                };
                const fitAddon = new FitAddon();
                terminals[action.payload.id].terminal.loadAddon(fitAddon);
                terminals[action.payload.id].terminal.loadAddon(new WebLinksAddon());
                terminals[action.payload.id].terminal.open(document.getElementById(`terminal-container-${action.payload.id}`)!);
                fitAddon.fit();
                document.querySelector('.xterm')!.addEventListener('wheel', e => {
                    if (terminals[action.payload.id].terminal.buffer.active.baseY > 0) {
                        e.preventDefault();
                    }
                });
                //TODO: chaine les promise
                setTimeout(() => {
                    terminals[action.payload.id].terminal.writeln("creating new session...");
                }, 500);
                setTimeout(() => {
                    terminals[action.payload.id].terminal.writeln('This is an info terminal.\r\nuse "help" to see the available commands');
                    store.dispatch(printPrompt(action.payload));
                }, 1500);
                terminals[action.payload.id].terminal.focus();


                // if (terminals[action.payload.id].socket) {
                //     terminals[action.payload.id].socket.on("connect", () => {
                //         store.dispatch(connectionEstablished(action.payload));
                //     })
                //     terminals[action.payload.id].socket.on("disconnect", () => {
                //         store.dispatch(connectionLost(action.payload));
                //     })
                //     terminals[action.payload.id].socket.on("cmdOutput", (data) => {
                //         store.dispatch(commandOutput({ id: action.payload.id, data: data }));
                //     })
                // }
            }
        }
        
        if (printPrompt.match(action)) {
            terminals[action.payload.id].terminal.write(terminals[action.payload.id].prompt);
        }

        if (commandInput.match(action)) {
            // terminals[action.payload.id].socket.emit("cmdInput", action.payload.data);
            for (let index = 0; index < action.payload.data.length - 1; index++) {
                terminals[action.payload.id].terminal.write('\b \b');
            }
            terminals[action.payload.id].terminal.writeln(action.payload.data);
            execCommand(terminals[action.payload.id].terminal, action.payload.data)
            store.dispatch(printPrompt(action.payload));
        }

        // if (commandOutput.match(action)) {
        //     terminals[action.payload.id].terminal.write(action.payload.data);
        // }

        if (onData.match(action)) {
            terminals[action.payload.id].terminal.onData(event => {
                switch (event) {
                    // Ctrl+C
                    case '\u0003': 
                        terminals[action.payload.id].terminal.writeln('^C');
                        // terminals[action.payload.id].socket.emit("interruptCommand");
                        store.dispatch(printPrompt(action.payload));
                        terminals[action.payload.id].buffer = "";
                    break;
                    // Ctrl-L
                    case '\u000C':
                        terminals[action.payload.id].terminal.write(ansiEscapes.clearScreen);
                        store.dispatch(printPrompt(action.payload));
                        terminals[action.payload.id].buffer = "";
                    break;
                    // Enter
                    case '\r':
                        if (terminals[action.payload.id].buffer !== "") {
                            store.dispatch(commandInput({ id: action.payload.id, data: terminals[action.payload.id].buffer + "\r" }));
                            terminals[action.payload.id].buffer = "";
                        }
                        else {
                            terminals[action.payload.id].terminal.writeln("");
                            store.dispatch(printPrompt(action.payload))
                        }
                    break;
                    // Backspace (DEL)
                    case '\u007F':
                        if (terminals[action.payload.id].buffer.length > 0) {
                            terminals[action.payload.id].terminal.write('\b \b');
                            terminals[action.payload.id].buffer = terminals[action.payload.id].buffer.substring(0, terminals[action.payload.id].buffer.length - 1);
                        }
                    break;
                    // Print all other characters for demo
                    default: 
                        if (event >= String.fromCharCode(0x20) && event <= String.fromCharCode(0x7E) || event >= '\u00a0') {
                            terminals[action.payload.id].buffer += event;
                            terminals[action.payload.id].terminal.write(event);
                        }
                }
            });
        }

        next(action);
    };
}

const baseTheme = {
    foreground: '#869ba9',
    background: '#1D1D1',
    selection: '#5DA5D533',
    black: '#1E1E1D',
    brightBlack: '#262625',
    red: '#CE5C5C',
    brightRed: '#FF7272',
    green: '#5BCC5B',
    brightGreen: '#72FF72',
    yellow: '#CCCC5B',
    brightYellow: '#FFFF72',
    blue: '#5D5DD3',
    brightBlue: '#7279FF',
    magenta: '#BC5ED1',
    brightMagenta: '#E572FF',
    cyan: '#5DA5D5',
    brightCyan: '#72F0FF',
    white: '#F8F8F8',
    brightWhite: '#FFFFFF',
    cursor: '#869ba9',
};
