import { useEffect } from 'react';

import ReduxSVG from '../assets/redux.svg';
import TypescriptSVG from '../assets/typescript.svg';
import JavascriptSVG from '../assets/logo-javascript.svg';
import DockerSVG from '../assets/docker-4.svg';
import ReactSVG from '../assets/react.svg';
import CsharpSVG from '../assets/c-sharp.svg';
import NestJs from '../assets/nestjs.svg';
import PostgresSVG from '../assets/postgresql.svg';
import DotnetSVG from '../assets/dot-net-core-7.svg';
import MuiSVG from '../assets/material-ui.svg';
import JestSVG from '../assets/jest-svg.svg';
import NodeSVG from '../assets/nodejs.svg';
import CplusplusSVG from '../assets/c.svg';
import CSVG from '../assets/c-1.svg';
import LinuxSVG from '../assets/Linux.svg';
import K8sSVG from '../assets/kubernets.svg';
import VagrantSVG from '../assets/vagrantup.svg';
import ArgoCD from '../assets/ArgoCD.svg';


import './TimeLine.css'
import Project from './Timeline/Project';

function TimeLine() {

    function reveal() {
        let reveals = Array.from(document.querySelectorAll(".direction-r"));
        reveals = reveals.concat(Array.from(document.querySelectorAll(".direction-l")));

        for (let i = 0; i < reveals.length; i++) {
            const windowHeight = window.innerHeight;
            const elementTop = reveals[i].getBoundingClientRect().top;
            const elementVisible = 200;
            if (elementTop < windowHeight - elementVisible) {
                reveals[i].classList.add("show");
            } else {
                reveals[i].classList.remove("show");
            }
        }
    }

    useEffect(() => {
        window.addEventListener('touchmove', reveal);
        window.addEventListener("scroll", reveal);
        reveal();

        return () => {
            window.addEventListener('touchmove', reveal);
            window.removeEventListener("scroll", reveal);
        }
    }, [])

    return (
        <div className='projects-container'>
            <h1 id="projects">Projets</h1>
            <div className='scrollContainer'>
                <div className='scrollLine'>
                    <div className='scrollProgress' />
                </div>
                <ul className="timeline">
                    <li id="TETRIS">
                        <Project
                            projectName={"Red Tetris"}
                            date={"2024"}
                            direction={'direction-r'}
                            xterm={false}
                            repoLink={"https://gitlab.com/ranuytte/42_red_tetris"}
                            demoLink={""}
                            techLogo={[JavascriptSVG, ReactSVG, ReduxSVG, NodeSVG, JestSVG]} 
                        >
                            L'objectif de ce projet est de développer un jeu Tetris multijoueur en utilisant une combinaison de technologies JavaScript full-stack.<br/><br/>
                            <b><i style={{ color: "#BFA181"}}>Projet en cours de réalisation..</i></b>
                        </Project>
                    </li>
                    <li id="IOT">
                        <Project
                            projectName={"Inception of Things"}
                            date={"2024"}
                            direction={'direction-l'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_iot"}
                            demoLink={""}
                            techLogo={[K8sSVG, VagrantSVG, ArgoCD]}
                        >
                            Projet d'introduction a <b>Kubernetes</b> qui vise à approfondir nos connaissances sur l'utilisation de <b>K3d</b> et <b>K3s</b> avec <b>Vagrant.</b> <br/><br/>
                            J'ai appris à configurer des machines virtuelles avec Vagrant, à utiliser K3s et son Ingress et à découvrir K3d pour simplifier l'installation de Kubernetes.<br/>
                        </Project>
                    </li>
                    <li id="LFS">
                        <Project
                            projectName={"Linux From Scratch"}
                            date={"2023"}
                            direction={'direction-r'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_matcha"}
                            demoLink={""}
                            techLogo={[LinuxSVG]}
                        >
                            Création d'une <b>distribution Linux</b> basique et fonctionnelle à partir de zéro.<br/><br/>
                            Les objectifs sont de <b>build un kernel Linux</b>, d'installer les paquets essentiels, d'implémenter un <b>système de fichiers</b> en suivant le standard (<b>FHS</b>).<br/>
                        </Project>
                    </li>
                    <li id="MATCHA">
                        <Project
                            projectName={"Matcha"}
                            date={"2023"}
                            direction={'direction-l'}
                            xterm={false}
                            repoLink={"https://gitlab.com/ranuytte/42_matcha"}
                            demoLink={"https://matcha.raphaelnuytten.fr"}
                            techLogo={[TypescriptSVG, ReactSVG, DockerSVG, CsharpSVG, DotnetSVG, PostgresSVG]}
                        >
                            Ce projet vise à créer une <b>application</b> de rencontre en ligne <b>sécurisée</b>, similaire aux sites existants tels que Tinder ou AdopteUnMec.
                            <br/><br/>
                            Les utilisateurs pourront s'inscrire, se connecter, créer leur profil, rechercher et consulter les profils d'autres utilisateurs, manifester leur intérêt par un "like" et discuter en temps réel si "match".
                        </Project>
                    </li>
                    <li id="TRANSCENDANCE">
                        <Project
                            projectName={"Transcendance"}
                            date={"2023"}
                            direction={'direction-r'}
                            xterm={false}
                            repoLink={"https://gitlab.com/ranuytte/42_transcendence"}
                            demoLink={"https://transcendence.raphaelnuytten.fr"}
                            techLogo={[TypescriptSVG, ReactSVG, MuiSVG, NestJs, DockerSVG, PostgresSVG]}
                        >
                            Ce projet nous propulse dans le <b>développement web</b> complet en nous demandant de construire un site web pour un <b>jeu de Pong multijoueur</b> en ligne.<br/><br/>
                            Mise en place d'une authentification via <b>OAuth 42</b>, d'un <b>système de matchmaking</b>, d'un <b>chat</b> et d'un système de <b>jeu en temps réel.</b>
                        </Project>
                    </li>
                    <li id="CONTAINERS">
                        <Project
                            projectName={"ft_containers"}
                            date={"2023"}
                            direction={'direction-l'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_containers"}
                            demoLink={""}
                            techLogo={[CplusplusSVG]}
                        >
                            Réimplémentation de certains conteneurs types de la bibliothèque <b>C++98</b> standard afin de consolider nos connaissances sur leur fonctionnement.<br/><br/>
                            L'objectif est de reproduire la structure de <b>vector</b>, <b>stack</b>, <b>map</b> et <b>set</b>.
                        </Project>
                    </li>
                    <li id="IRC">
                        <Project
                            projectName={"ft_irc"}
                            date={"2022"}
                            direction={'direction-r'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_irc"}
                            demoLink={""}
                            techLogo={[CplusplusSVG]}
                        >
                            Création de notre propre serveur <b>IRC</b> en C++98 dans le respect de la <b>RFC.</b><br/><br/>
                            Les utilisateurs pourront se connecter et interagir en utilisant un programme client IRC distinct.
                        </Project>
                    </li>
                    <li id="MINISHELL">
                        <Project
                            projectName={"Minishell"}
                            date={"2022"}
                            direction={'direction-l'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_minishell"}
                            demoLink={""}
                            techLogo={[CSVG]}
                        >
                            Ce projet vous propose de créer un mini-shell, à l'image de <b>Bash</b>.<br/><br/>
                            On y apprend ainsi davantage sur les <b>processus</b> et les <b>descripteurs de fichiers</b>.
                        </Project>
                    </li>
                    <li id="PUSHSWAP">
                        <Project
                            projectName={"Push_swap"}
                            date={"2022"}
                            direction={'direction-r'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_push_swap"}
                            demoLink={""}
                            techLogo={[CSVG]}
                        >
                            Développer un programme capable de <b>trier des données sur une pile</b> en utilisant un ensemble d'<b>instructions limité</b> et en <b>minimisant le nombre d'opérations nécessaires.</b>
                        </Project>
                    </li>
                    <li id="PHILOSOPHERS">
                        <Project
                            projectName={"Philosophers"}
                            date={"2022"}
                            direction={'direction-l'}
                            xterm={true}
                            repoLink={"https://gitlab.com/ranuytte/42_philosophers"}
                            demoLink={""}
                            techLogo={[CSVG]}
                        >
                            Ce projet nous plonge dans les méandres de la programmation <b>multi-threadée</b> en simulant un dîner de philosophes.<br/><br/>
                            On y découvre la création de <b>threads</b> et l'utilisation de <b>mutex</b> pour synchroniser l'accès à des ressources partagées.
                        </Project>
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default TimeLine
