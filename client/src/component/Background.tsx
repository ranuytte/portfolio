import './Background.css';

import { useEffect, useState } from 'react';

function Background() {
    const [numberOfCircles, setNumberOfCircles] = useState<number>(10);

    const circles = Array.from({ length: numberOfCircles }, (_, index) => (
        <div key={index} className={`circle-${index + 6}`} />
    ));

    useEffect(() => {
        const updateNumberOfCircles = () => {
            const largeurEcran = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            switch (true) {
                case largeurEcran < 1200:
                    setNumberOfCircles(8);
                break;
                case largeurEcran > 1200:
                    setNumberOfCircles(6);
                break;
                default:
                    break;
            }
        };
        window.addEventListener('resize', updateNumberOfCircles);
        updateNumberOfCircles();

        return () => {
            window.removeEventListener('resize', updateNumberOfCircles);
        };
    }, []);

    return (
        <div className='circle-body'>
            {circles}
        </div>
    );
}

export default Background;
