// import ansiEscapes from 'ansi-escapes';
import { useEffect } from 'react'
import { useAppDispatch } from '../app/hooks';
import { initTerm, onData } from '../features/termSlice';

import './Xterm.css';
// https://xtermjs.org/js/demo.js

interface XtermProps {
    terminalId: string;
}

function Xterm({terminalId}: XtermProps) {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(initTerm({ id: terminalId, cmdInput: "", cmdOutput: "", isConnected: false }));
        dispatch(onData({ id: terminalId }));
    }, [dispatch, terminalId])

    return (
        <>
            <div id={`terminal-container-${terminalId}`}>
                <div id='terminal'></div>
            </div>
        </>
    )
}

export default Xterm
