import { ReactNode, useState } from 'react';
import GitlabSVG from '../../assets/gitlab-3.svg';
import RocketPNG from '../../assets/startup.png';
import './Project.css';

interface ProjectProps {
    projectName: string;
    date: string;
    direction: string;
    xterm: boolean;
    techLogo: string[];
    repoLink: string;
    demoLink: string;
    children: ReactNode;
}

function Project({projectName, date, direction, demoLink, repoLink, xterm, techLogo, children}: ProjectProps) {
    const [isFlipped, setIsFlipped] = useState<boolean>(false);

    const techImg = techLogo.map((value, index) => 
        <img key={index} src={value} alt={value} width="30px" height="30px" />
    );

    function Handle() {
        if (xterm === true)
            setIsFlipped(!isFlipped);
    }

    return (
        <>                        
            <div className={`card ${isFlipped ? 'is-flipped' : ''}`}>
                <div className="front">
                    <div className={direction}>
                        <div className="flag-wrapper">
                            <span className="flag" data-content='2024'>{projectName}</span>
                            <span className="time-wrapper"><span className="time">{date}</span></span>
                        </div>
                        <div className='container'>
                            <div className='tech'>
                                {techImg}
                            </div>
                            <div className="desc">
                                {children}
                            </div>
                            <div className='view'>
                                {!xterm && demoLink !== "" &&
                                <a href={demoLink} target="_blank" rel="noopener noreferrer">
                                    <button className='btn' id='rocket' onClick={Handle}>
                                        <img src={RocketPNG} alt="Rocket PNG" />
                                        <span>Live Demo</span>
                                    </button>
                                </a>
                                }
                                <a href={repoLink} target="_blank" rel="noopener noreferrer">
                                    <button className='btn'>
                                        <img src={GitlabSVG} alt="Gitlab Logo" />
                                        <span>Code Source</span>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                {/* {isFlipped === true ? */}
                {/*     <div className='back'> */}
                {/*         <div className='showTerm'> */}
                {/*             <Xterm terminalId={projectName} /> */}
                {/*         </div> */}
                {/*     </div> */} 
                {/*     : */}
                {/*     <></> */}
                {/* } */}
            </div>
        </>
    );
}

export default Project
