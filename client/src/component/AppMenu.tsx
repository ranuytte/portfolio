import { useCallback, useEffect, useState } from 'react';
import GitlabSVG from '../assets/gitlab-3.svg';
import LinkedinSVG from '../assets/linkedin.png';

import './AppMenu.css';

function AppMenu() {
    const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    //prevents opening of CV modal
    const navigateTo = () => {
        const monProjetElement = document.getElementById('PHILOSOPHERS');
        monProjetElement?.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
    };

    const toggleMenuOnScroll = useCallback(() => {
        if (window.scrollY < 20)
            return;
        if (isMenuOpen === true)
            setIsMenuOpen(false);
    }, [isMenuOpen]);

    const toggleMenuOnEsc = useCallback((event: KeyboardEvent) => {
        if (isMenuOpen === true)
            if (event && event.key === "Escape")
                setIsMenuOpen(false);
    }, [isMenuOpen]);

    useEffect(() => {
        window.addEventListener('touchmove', toggleMenuOnScroll);
        window.addEventListener('scroll', toggleMenuOnScroll);
        window.addEventListener('keydown', toggleMenuOnEsc);

        return () => {
            window.addEventListener('touchmove', toggleMenuOnScroll);
            window.removeEventListener("scroll", toggleMenuOnScroll);
            window.addEventListener('keydown', toggleMenuOnEsc);
        }
    }, [toggleMenuOnScroll, toggleMenuOnEsc])

    return (
        <div className='appMenu'>
            <div className="hamburger-menu">
                <button className={`menu__btn ${isMenuOpen ? 'open' : ''}`} onClick={toggleMenu}> 
                    <span></span>
                </button>
                <ul className={`menu__box ${isMenuOpen ? 'open' : ''}`}>
                    <li><a className="menu__item" href={`${window.location.origin}${"/download/CV.pdf"}`} target="_blank">Curriculum Vitæ</a></li>
                    <li><a className="menu__item" href="mailto:contact@raphaelnuytten.fr">Contact</a></li>
                    <li><a className="menu__item" href="#projects">Projets</a></li>
                    <ul className="submenu">
                        <li><a className="submenu__item" href="#TETRIS">Red Tetris</a></li>
                        <li><a className="submenu__item" href="#IOT">Inception of Things</a></li>
                        <li><a className="submenu__item" href="#LFS">Linux From Scratch</a></li>
                        <li><a className="submenu__item" href="#MATCHA">Matcha</a></li>
                        <li><a className="submenu__item" href="#TRANSCENDANCE">Transcendance</a></li>
                        <li><a className="submenu__item" href="#CONTAINERS">ft_containers</a></li>
                        <li><a className="submenu__item" href="#IRC">ft_irc</a></li>
                        <li><a className="submenu__item" href="#MINISHELL">Minishell</a></li>
                        <li><a className="submenu__item" href="#PUSHSWAP">Push_swap</a></li>
                        <li><a className="submenu__item" onClick={navigateTo}>Philosophers</a></li>
                    </ul>
                </ul>
            </div>
            <h1>Raphael Nuytten</h1>
            <div className='appMenu-link'>
                <a href='https://gitlab.com/ranuytte' target="_blank" rel="noopener noreferrer" title='Gitlab'>
                    <img className='gitlabLogo' src={GitlabSVG} alt="Allez sur Gitlab" />
                </a>
                <a href='https://www.linkedin.com/in/rapha%C3%ABl-nuytten/' target="_blank" rel="noopener noreferrer" title='Linkedin'>
                    <img className='linkedinLogo' src={LinkedinSVG} alt="Allez sur Linkedin" />
                </a>
            </div>
        </div>
    );
}

export default AppMenu
