import TestCV from '../assets/CV.pdf';
import './DisplayCV.css';

import { useCallback, useEffect, useState } from 'react';


function DisplayCV() {
    const [oldScroll, setOldScroll] = useState<number>(0);

    const showModal = useCallback(() => {
        const modal = Array.from(document.querySelectorAll(".modalCV"));
        const windowScroll = window.scrollY;
        const tailleContenuTotal = document.body.scrollHeight;

        if (oldScroll > windowScroll) {
            if ((windowScroll + window.innerHeight) < (tailleContenuTotal - 50)) {
                modal[0].classList.remove("show")
            }
        }
        else {
            if ((windowScroll + window.innerHeight) >= (tailleContenuTotal - 5)) {
                modal[0].classList.add("show")
            }
        }
        setOldScroll(windowScroll);
    }, [oldScroll]);

    useEffect(() => {
        window.addEventListener('touchmove', showModal);
        window.addEventListener('scroll', showModal);

        return () => {
            window.removeEventListener('touchmove', showModal);
            window.removeEventListener('scroll', showModal);
        };
    }, [showModal]);

    return (
        <>
            <div className='modalCV' id='CV'>
                <h1>Curriculum Vitæ</h1>
                <object data={TestCV} type="application/pdf" width="90%" height="100%">
                    <span />
                    <p id="oops">Oops!</p>
                    <p>Your browser doesn't support PDFs!</p>
                    <a className='displayCV' href={TestCV} download="Raphael_NUYTTEN">Télécharger le CV</a>
                </object>
                <a className='displayCV' href="mailto:contact@raphaelnuytten.fr">Contactez-moi</a>
            </div>
        </>
    );
}

export default DisplayCV
