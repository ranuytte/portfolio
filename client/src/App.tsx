import ScrollUp from './assets/scroll-up.png';
import './App.css'

import AppMenu from './component/AppMenu';
import TimeLine from './component/TimeLine'
import Xterm from './component/Xterm'
import DisplayCV from './component/DisplayCV';
import Background from './component/Background';

function App() {
        const handleScrollToBottom = () => {
            window.scrollTo({
                top: document.body.scrollHeight,
                behavior: 'smooth',
            });
        };

    return (
        <>
            <div className='appContainer'>
                <AppMenu />
                <div className='circle'>
                    <div className='circle-1' />
                    <div className='circle-2' />
                    <div className='circle-3' />
                    <div className='circle-4' />
                    <div className='circle-5' />
                </div>
                <Xterm terminalId={"Home"}/>
                <TimeLine />
                <a href="#CV" onClick={handleScrollToBottom} className="scroll-down-button"></a>
                <a href="#CV" onClick={handleScrollToBottom} className="scroll-down-button"></a>
                <a href="#CV" onClick={handleScrollToBottom} className="scroll-down-button"></a>
                <DisplayCV />
                <Background />
            </div>
            <a href="#" className="backToTop">
                <img src={ScrollUp} alt='Scroll Up' />
            </a>
        </>
    )
}

export default App
